# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Carbono Digital Selection Test
* Used Technologies: Laravel, VueJs, Bootstrap, SASS

### How do I get set up? ###

* Run `composer install`
* Setup a virtualhost for the project (public/ directory)
* To do styling modifications using SASS, add sass --watch to scss files: `sass --watch public/css/scss/:public/css/ --style compressed`

### Who do I talk to? ###

* Andsalves
* ands.alves.nunes@gmail.com