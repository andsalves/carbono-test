var vueElSelector = '#vue-products-list';

var vueProducts = new Vue({
    el: vueElSelector,
    data: {
        products: [],
        filteredProducts: [],
        loading: true,
        error: null,
        search: ''
    },
    methods: {
        loadProducts: function () {
            var self = this;
            self.loading = true;
            self.error = null;

            this.$http.get('/api/products').then(function (response) {
                if (response.status === 200 && typeof(response.body) !== 'undefined') {
                    self.products = response.body.products.items;
                } else {
                    self.error = 'Error Loading products!';
                }

                self.loading = false;
                self.filterProducts();
            }, function (response) {
                self.error = 'Error Loading products!';
                self.loading = false;
                console.error(response);
            });
        },
        filterProducts: function () {
            this.filteredProducts = [];
            this.search = this.search.toLowerCase();

            for (var i = 0; i < this.products.length; i++) {
                var nameLowerCase = this.products[i].product.name.toLowerCase();

                switch (true) {
                    case this.search === '':
                    case nameLowerCase.match(this.search) !== null:
                    case String(1000*this.products[i].product.price.aluguel).match(this.search) !== null:
                    case String(this.products[i].product.area).match(this.search) !== null:
                    case String(this.products[i].product.id).match(this.search) !== null:
                        this.filteredProducts.push(this.products[i].product);
                        break;

                }
            }
        },
        created: function () {
            var element = document.querySelector(vueElSelector);

            if (element) {
                var notLoadedPlaceholders = document.querySelectorAll('.vue-not-loaded');
                for (var i = 0; i < notLoadedPlaceholders.length; i++) {
                    notLoadedPlaceholders[i].style.display = 'none';
                }

                element.style.display = 'block';
            }

            this.loadProducts();
        }
    }
});

vueProducts.created();