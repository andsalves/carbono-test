<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

/**
 * @author Andsalves
 */
class IndexController extends BaseController {

    public function index() {
        return view('products/index');
    }

}
