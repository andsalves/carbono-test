<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

/**
 * @author Andsalves
 */
class ProductsController extends BaseController {

    public function index() {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => config('app.products_source_url')
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return response()->json([
            'products' => @json_decode($response, true)
        ]);
    }

}
