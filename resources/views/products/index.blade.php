@extends('layouts/carbono')

@section('content')
    <div class="products-list" id="vue-products-list" style="display: none" xmlns:v-bind="http://www.w3.org/1999/xhtml" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <section class="search-section">
            <label>Busca &nbsp;</label>
            <input name="search" title="Busca" v-model="search" v-on:keydown="filterProducts" class="search-input"/>
        </section>

        <ul class="products-items">
            <li class="col-md-4 col-sm-6 float-left" v-for="product in filteredProducts" v-if="!loading">
                <h4> @{{product.name}} </h4>
                <div class="product-item-body float-left">
                    <ul class="mini-gallery float-left">
                        <li v-for="image in product.images" class="col-sm-6 float-left">
                            <img :src="image" alt="Image" onerror="this.src='{{url("/images/no_image.png")}}'"/>
                        </li>
                    </ul>

                    <div class="float-left col-sm-12 px-0">
                        <hr class="col-sm-11 mx-auto px-0 mb-1">
                    </div>

                    <div class="product-values float-left col-sm-12 px-0">
                        <div class="col-sm-11 mx-auto px-0">
                            <label class="area text-left col-sm-6 px-0 float-left">@{{product.area}} m²</label>
                            <label class="price text-right col-sm-6 px-0 float-left">@{{1000*product.price.aluguel}}</label>
                            <label class="code text-center col-sm-12 px-0 float-left">Cód.: @{{product.id}}</label>
                        </div>
                    </div>

                    <label v-if="product.destaque" class="featured"> Destaque </label>
                </div>
            </li>
        </ul>

        <div v-if="loading" class="row col-sm-12">
            <img src="/images/loading.gif" height="100" alt="Loading..." style="margin: 0 auto">
        </div>
    </div>
    <!-- While Vue is not loaded yet, show this -->
    <div class="row col-sm-12 vue-not-loaded">
        <img src="/images/loading.gif" height="100" alt="Loading..." style="margin: 0 auto">
    </div>

    <script type="application/javascript" src="/js/products-vue.js"></script>
@endsection