<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Carbono Test </title>

    <link rel="stylesheet" href="/css/bootstrap4.0.0-alpha.min.css">
    <link rel="stylesheet" href="/css/carbono.css">

    <script src="/js/vue.min.js" type="text/javascript"></script>
    <script src="/js/vue.resource-1.3.4.js" type="text/javascript"></script>
</head>
<body>

<header class="header">
    <img class="carbono-logo flex-center position-ref text-center" src="/images/carbono-logo.png" />
</header>

<article class="content mx-auto d-block col-sm-12">
    @yield('content')
</article>

<footer>
</footer>

</body>
</html>
